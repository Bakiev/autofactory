package com.example.autofactory.test.factory

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.auto.factory.AutoFactory
import dagger.Component
import javax.inject.Inject
import javax.inject.Singleton

class FactoryActivity : AppCompatActivity() {

    @Inject
    lateinit var helperFactory: HelperFactory
    // Uncomment the line below (and comment out the line above) to see the workaround for dagger 2.36
//    lateinit var helperFactory: com.example.autofactory.test.factory.HelperFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerFactoryComponent.builder().build().inject(this)
        super.onCreate(savedInstanceState)
    }
}

@Singleton
@Component
internal interface FactoryComponent {

    fun inject(activity: FactoryActivity)
}

@AutoFactory
class Helper
