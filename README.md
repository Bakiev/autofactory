### Autofactory example project

The project demonstrates "cannot find symbol" issue when [Autofactory](https://github.com/google/auto/tree/master/factory) is being used together with the latest [Dagger](https://github.com/google/dagger) library in an Android library module. This is a [dagger ticket](https://github.com/google/dagger/issues/2702)

### To reproduce:

- Run `./gradlew :factory:kaptDebugKotlin`

### Notes:
- Can't be reproduced with dagger 2.25.2 and below
- Can't be reproduced if `FactoryActivity.kt` is located in the "app" module
- Can't be reproduced with dagger 2.36 if FQDN specified for the `XYZFactory` class - see comments in the `FactoryActivity.kt`